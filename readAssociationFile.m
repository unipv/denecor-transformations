%  Copyright 2017 Universita' degli Studi di Pavia
%  Computer Vision and Multimedia Laboratory
%  http://vision.unipv.it
% 
%  This program is free software; you can redistribute it and/or modify
%  it under the terms of the GNU General Public License as published by
%  the Free Software Foundation; either version 2 of the License, or
%  (at your option) any later version.
% 
%  This program is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
% 
%  You should have received a copy of the GNU General Public License along
%  with this program; if not, write to the Free Software Foundation, Inc.,
% 
%  Authors: giulia.matrone@unipv.it, marco.piastra@unipv.it

function structArray = readAssociationFile(varargin)

if nargin > 0
    path = varargin{1};
else
    path = '';
end
if nargin > 1
    filename = varargin{2};
else
    filename = '';
end

if isempty(filename)
    [filename, path] = uigetfile('.csv','Select the association file:', path);    
end

fid = fopen(fullfile(path, filename), 'r');
line = fgetl(fid);
structArray = [];
while ischar(line)
    fields = strsplit(line,',');
    structArray = [structArray; fields];
    line = fgetl(fid);
end
fclose(fid);

end

