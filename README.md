DeNeCoR Project: computing 3D transformations
=============================================

This software package is intended as supplementary material to:

*Matrone, G., Ramalli, A., Savoia, A., Quaglia, F., 
Castellazzi G., Morbini, P., Piastra, M.*

**An experimental protocol for assessing the performance of new ultrasound 
probes based on CMUT technology in application to brain imaging**

Submitted to [JoVE](https://www.jove.com/)

## Usage Guide

This software package shows how to compute all rigid 3D transformations described
in the above article.

In particular, there are three such transformations:

- from the Ultrasound probe space to the marker space (**U2M**)
- from the marker space to the field of view of NDI Polaris Vicra (**M2V**)
- from the field of view of NDI Polaris Vicra to a specific 3D MRI image (**V2MRI**)
  
Of these three transformations, U2M and V2MRI must be pre-computed from calibration
procedures and they depend, respectively, on the specific mounting of the reflective
markers on the probe and on the specific 3D MRI image being considered.  

The M2V transformation, on the other hand, varies over time and can be computed
directly from the data returned in real-time by the NDI Polaris Vicra motion
tracker.

Altogether these three transformations are composed, per each Ultrasound image, to 
obtain the desired Ultrasound to MRI (**U2MRI**) image registration.  

NOTE: except stated otherwise, all transformations are stored as a rotation matrix
plus a translation vector

### Pre-requisites

- [MATLAB](https://it.mathworks.com/products/matlab.html)

The code has been developed with version R2013a and is certainly compatible with more
recent versions (at least R2017a included). 

- [ParaView 5.0](http://www.paraview.org/)
For the visualization of registered Ultrasound and MR images 

### Installation

```
git clone https://bitbucket.org/unipv/denecor-transformations
```

Assume that <TRANSFORMATIONS_HOME> is now the installation directory 

### Startup

Start MATLAB and go to the directory <TRANSFORMATIONS_HOME>

### Computing calibrations (U2M)

Run
```
calibration_u2m.m  
```
At the request dialog, select the associations file:
```
%TRANSFORMATIONS_HOME%\Calibration_MF1_1\Calibration_MF1_1_u2m.csv  
```
The procedure runs automatically.

#### Inputs

- the pivoting file for pointer calibration
```
%TRANSFORMATIONS_HOME%\Calibration_MF1_1\logfiles\pivoting.csv  
```
- the pointer positions acquired manually
```
%TRANSFORMATIONS_HOME%\Calibration_MF1_1\logfiles\log_arturablock_puntaleMF1_P{0|1}.txt
```
- the positions of the spheres as manually registered in Ultrasound images 
```
%TRANSFORMATIONS_HOME%\Calibration_MF1_1\P{0|0bis|1|1bis}_SliceQ_S{1|2|3}.csv
```
- the actual Ultrasound images (for timestamp) 
```
%TRANSFORMATIONS_HOME%\Calibration_MF1_1\P{0|0bis|1|1bis}_SliceIQ.vtk
```
- the NDI Polaris Vicra logs for corresponding marker positions 
```
%TRANSFORMATIONS_HOME%\Calibration_MF1_1\logfiles\log_arturablock_MF1_P{0|1}txt  
```

#### Outputs

- the U2M transformation matrix, both in MATLAB and ASCII format
```
%TRANSFORMATIONS_HOME%\Calibration_MF1_1\Calibration_MF1_1_u2m_u2m.mat
%TRANSFORMATIONS_HOME%\Calibration_MF1_1\Calibration_MF1_1_u2m_ASCII_u2m.mat
```
NOTE: these output files must be copied to
```
%TRANSFORMATIONS_HOME%\Acqusition_MF1_1\  
```

### Computing transformations (V2MRI)

Run
```
transformation_v2mri.m  
```
At the request dialog, select the associations file:
```
%TRANSFORMATIONS_HOME%\Acqusition_MF1_1\Acquisition_MF1_1_transformation_v2mri.csv  
```
The procedure runs automatically.

#### Inputs
- the pivoting file for pointer calibration (i.e. same as above)
```
%TRANSFORMATIONS_HOME%\Acqusition_MF1_1\logfiles\pivoting.csv 
```
- the positions of the spheres as manually registered in the target MRI image 
```
set TARGET_MRI_NAME=6_t2_spc_sag_p2_iso_1.0
%TRANSFORMATIONS_HOME%\Acqusition_MF1_1\%TARGET_MRI_NAME%_P{0|1|2|3|4|5}S{0|1|2}.csv
```
- the NDI Polaris Vicra logs for corresponding pointer positions 
```
%TRANSFORMATIONS_HOME%\Acqusition_MF1_1\logfiles\log_arturino_puntale_MF1.txt  
```

#### Outputs

- the V2MRI transformation matrix, both in MATLAB and ASCII format
```
%TRANSFORMATIONS_HOME%\Acqusition_MF1_1\Acquisition_MF1_1_transformation_v2mri_v2mri.mat
%TRANSFORMATIONS_HOME%\Acqusition_MF1_1\Acquisition_MF1_1_transformation_v2mri_ASCII_v2mri.mat
```
NOTE: these output files are also inputs to the procedure below 

### Computing image registrations (U2MRI)

Run
```
registration_u2mri.m  
```
At the request dialog, select the associations file:
```
%TRANSFORMATIONS_HOME%\Acqusition_MF1_1\Acquisition_MF1_1_registration_u2mri.csv  
```
The procedure runs automatically.

#### Inputs

- the U2M calibration (see above)
```
%TRANSFORMATIONS_HOME%\Acqusition_MF1_1\Calibration_MF1_1_u2m_u2m.mat 
```
- the V2MRI transformation (see above)
```
%TRANSFORMATIONS_HOME%\Acqusition_MF1_1\Acquisition_MF1_1_transformation_v2mri_v2mri.mat 
```
- the actual Ultrasound images (for timestamp) 
```
%TRANSFORMATIONS_HOME%\Acqusition_MF1_1\Pose{1L|1R|2L|2R|3L|3R|4L|4R|5L|5R|6|7}_SliceIQ{|F2}.vtk
```
- the NDI Polaris Vicra logs for corresponding pointer positions 
```
%TRANSFORMATIONS_HOME%\Acqusition_MF1_1\logfiles\log_poses_and_fh_MF1.txt  
```

#### Outputs

- the U2MRI transformation matrix, per each image, in both ASCII and ParaView format
```
%TRANSFORMATIONS_HOME%\Acqusition_MF1_1\Pose{1L|1R|2L|2R|3L|3R|4L|4R|5L|5R|6|7}_SliceIQ{|F2}_%TARGET_MRI_NAME%_u2mri_ASCII.mat
%TRANSFORMATIONS_HOME%\Acqusition_MF1_1\Pose{1L|1R|2L|2R|3L|3R|4L|4R|5L|5R|6|7}_SliceIQ{|F2}_%TARGET_MRI_NAME%_u2mri.csv
```
NOTE: in the transformations for ParaView in the .csv files rotations are expressed 
in Euler angles with the specific application conventions     

NOTE: the procedure is relatively slow (i.e. around 30 s per image on a fast workstation)
due to the need, per each image, to parse and scan a text file of 15MB containing the
NDI Polaris Vicra log. Apart from that, the actual computation is extremely fast. 

NOTE: Ultrasound binary images are *NOT* processed during this phase, only the
transformation matrices are computed. The actual image registration is performed by
ParaView, at visualization time (see below)

### Visualization in ParaView

#### Load the 3D MR image

- Start ParaView
- open the dialog File > Open
- select the directory '6_t2_spc_sag_p2_iso_1.0' provided
- select the block of .dcm files and click 'OK'
- select 'DICOM Files (Directory)'
- click on 'Apply' in the Properties pane, to load the DICOM image
- select Representation: 'Volume'

You may want to adjust both the color map and the level of transparence:

- click on 'Coloring: Edit' button to open the 'Color Map Editor'
- use the 'Choose Preset' button to select 'X Ray' standard graylevel color pattern 

#### Load an Ultrasound image

- open the dialog File > Open
- select the Ultrasound image of choice, for instance:
```
%TRANSFORMATIONS_HOME%\Acqusition_MF1_1\pose1L_SliceIQ.vtk
```
- click on 'Apply' in the Properties pane, to load the VTK image

The image will appear in the main view at the origin of the MR space.

NOTE: multiple Ultrasound images can be loaded and registered in the same session. 

#### Register an Ultrasound image

- in the Pipeline Browser, select the Ultrasound image to be registered
- select Filter > Alphabetical > Transform to apply a rigid 3D transformation to
the image selected
- in a text editor, open the corresponding registration file, for instance:
```
%TRANSFORMATIONS_HOME%\Acqusition_MF1_1\Pose1L_SliceIQ_%TARGET_MRI_NAME%_u2mri.csv
```
- manually copy the transformation data into the Properties of the Transform
(first line in the .csv file: translate; second line: rotate; leave scale at 1)
- click on 'Apply' to confirm the transformation

The Ultrasound image will appear as superimposed to the MR image in the registered
position. Further visualization actions can be performed via ParaView commands 
(e.g. oriented slicing).

![Screen Sample](registered_image.png)

### Pre-recorded data

These files contain pre-recorded data:

- directory '6_t2_spc_sag_p2_iso_1.0' contains an MR image of a bovine brain fixated 
in formalin
- directories 'Calibration_MF1_1/logfiles' and 'Acqusition_MF1_1/logfiles' contain 
log files recording the outputs produced by NDI Polaris Vicra during the actual 
experimental sessions with a pointer and a CMUT Ultrasound probe 
- directory 'Calibration_MF1_1' contains .csv files reporting the position of 
spheres in manually registered Ultrasound images 
- directory 'Acqusition_MF1_1' contains .csv files reporting the position of 
spheres in the manually registered MR image included 
- directory 'Acqusition_MF1_1' contains .vtk files of the binary images acquired
with the CMUT Ultrasound probe for each virtual target (see also 
[DeNeCoR-tracking](https://bitbucket.org/unipv/denecor-tracking) repository) 


### Acknowledgements

This work has been partially supported by the National governments and 
the European Union through the ENIAC JU project DeNeCoR under grant agreement
number 324257.
