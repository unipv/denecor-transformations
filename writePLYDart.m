%  Copyright 2017 Universita' degli Studi di Pavia
%  Computer Vision and Multimedia Laboratory
%  http://vision.unipv.it
% 
%  This program is free software; you can redistribute it and/or modify
%  it under the terms of the GNU General Public License as published by
%  the Free Software Foundation; either version 2 of the License, or
%  (at your option) any later version.
% 
%  This program is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
% 
%  You should have received a copy of the GNU General Public License along
%  with this program; if not, write to the Free Software Foundation, Inc.,
% 
%  Authors: giulia.matrone@unipv.it, marco.piastra@unipv.it


function writePLYDart(filename, v, R, T, length)

vZ = v * R' + T;
vX = [length 0 0] * R' + T;
vY = [0 length 0] * R' + T;

% Open the file.
fid = fopen(filename, 'w');
if fid == -1
    error('Cannot open file for writing.');
end

% New line.
nl = sprintf('\n'); % Stupid matlab doesn't interpret \n normally.

% Write the file header.
fwrite(fid, ['ply' nl ...
    'format ascii 1.0' nl ...
    'comment MATLAB generated' nl ...
    'element vertex ' num2str(4) nl ...
    'property float x' nl ...
    'property float y' nl ...
    'property float z' nl ...
    'element face ' num2str(2) nl ...
    'property list uchar int vertex_indices' nl ...
    'end_header' nl
]);

% vertices
fprintf(fid, '%f %f %f\n', T(1), T(2), T(3));
fprintf(fid, '%f %f %f\n', vX(1), vX(2), vX(3));
fprintf(fid, '%f %f %f\n', vY(1), vY(2), vY(3));
fprintf(fid, '%f %f %f\n', vZ(1), vZ(2), vZ(3));

% faces
fprintf(fid, '3 0 1 3\n');
fprintf(fid, '3 0 3 2\n');

% Close the file.
fclose(fid);

end