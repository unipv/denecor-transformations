%  Copyright 2017 Universita' degli Studi di Pavia
%  Computer Vision and Multimedia Laboratory
%  http://vision.unipv.it
% 
%  This program is free software; you can redistribute it and/or modify
%  it under the terms of the GNU General Public License as published by
%  the Free Software Foundation; either version 2 of the License, or
%  (at your option) any later version.
% 
%  This program is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
% 
%  You should have received a copy of the GNU General Public License along
%  with this program; if not, write to the Free Software Foundation, Inc.,
% 
%  Authors: giulia.matrone@unipv.it, marco.piastra@unipv.it

% Average position in a dataset given initial and final positions
function [x, y, z, q0, qx, qy, qz] = averageHandlePositions(handleData, id_start, id_end)
% x,y,z = 3D coordinates [mm]
% q0,qx,qy,qz = quaternions
% handleData = handle data from Vicra
% id_start, id_end = data interval

if isempty(handleData)
    error('No valid values in this handle dataset for the given interval');
end

x = zeros(length(id_start),1);
y = zeros(length(id_start),1);
z = zeros(length(id_start),1);
q0 = zeros(length(id_start),1);
qx = zeros(length(id_start),1);
qy = zeros(length(id_start),1);
qz = zeros(length(id_start),1);

%average values (over dt)
for i=1:length(id_start)
    handleDataWindow = handleData(id_start(i):id_end(i),:);
    handleDataWindow(handleDataWindow(:,2) == 1,:) = []; % remove missing records
    
    if isempty(handleDataWindow)
        error('No valid values in this handle dataset for the given interval');
    end

    x(i) = mean( handleDataWindow(:,7) ); %[mm]
    y(i) = mean( handleDataWindow(:,8) );
    z(i) = mean( handleDataWindow(:,9) );
        
    % Averaging quaternions requires a different procedure
    qts = handleDataWindow(:,3:6);
    qavg = avg_quaternion_markley(qts);
    
    q0(i) = qavg(1);
    qx(i) = qavg(2);
    qy(i) = qavg(3);
    qz(i) = qavg(4);
end
