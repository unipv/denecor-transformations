%  Copyright 2017 Universita' degli Studi di Pavia
%  Computer Vision and Multimedia Laboratory
%  http://vision.unipv.it
% 
%  This program is free software; you can redistribute it and/or modify
%  it under the terms of the GNU General Public License as published by
%  the Free Software Foundation; either version 2 of the License, or
%  (at your option) any later version.
% 
%  This program is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
% 
%  You should have received a copy of the GNU General Public License along
%  with this program; if not, write to the Free Software Foundation, Inc.,
% 
%  Authors: giulia.matrone@unipv.it, marco.piastra@unipv.it


function saveASCII(filename, R, T)
% saveASCII(filename, R, T)
% a .mat file in the same format as PVABRAIN
%

% Open the file.
fid = fopen(filename, 'w');
if fid == -1
    error('Cannot open file for writing.');
end

fprintf(fid, '%f %f %f %f\n', R(1,1), R(1,2), R(1,3), T(1));
fprintf(fid, '%f %f %f %f\n', R(2,1), R(2,2), R(2,3), T(2));
fprintf(fid, '%f %f %f %f\n', R(3,1), R(3,2), R(3,3), T(3));
fprintf(fid, '0 0 0 1\n');

% Close the file.
fclose(fid);

end