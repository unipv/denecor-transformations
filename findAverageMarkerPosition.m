%  Copyright 2017 Universita' degli Studi di Pavia
%  Computer Vision and Multimedia Laboratory
%  http://vision.unipv.it
%
%  This program is free software; you can redistribute it and/or modify
%  it under the terms of the GNU General Public License as published by
%  the Free Software Foundation; either version 2 of the License, or
%  (at your option) any later version.
%
%  This program is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
%
%  You should have received a copy of the GNU General Public License along
%  with this program; if not, write to the Free Software Foundation, Inc.,
%
%  Authors: giulia.matrone@unipv.it, marco.piastra@unipv.it

% PROBE POSITION
function [X, Y, Z, q0, qx, qy, qz] = findAverageMarkerPosition(path, filename, stime, etime)

% PARSE LOG FILE
[~, handle2, timeseries] = parseLogfile(path, filename);

% find the interval which matches the desired 3D image frame (if any)
[id_start, id_end] = matchTimestamp(timeseries, stime, etime);

% ACQUIRED DATA (HANDLE 2)
[X, Y, Z, q0, qx, qy, qz] = averageHandlePositions(handle2, id_start, id_end); 

