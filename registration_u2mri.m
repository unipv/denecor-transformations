%  Copyright 2017 Universita' degli Studi di Pavia
%  Computer Vision and Multimedia Laboratory
%  http://vision.unipv.it
% 
%  This program is free software; you can redistribute it and/or modify
%  it under the terms of the GNU General Public License as published by
%  the Free Software Foundation; either version 2 of the License, or
%  (at your option) any later version.
% 
%  This program is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
% 
%  You should have received a copy of the GNU General Public License along
%  with this program; if not, write to the Free Software Foundation, Inc.,
% 
%  Authors: giulia.matrone@unipv.it, marco.piastra@unipv.it


%% Complete registration of images (U2MRI) from multiple acquistions

if size(dbstack,1)==1
    % Direct call
    clear global
    clear variables
    close all
    clc
end

[filename, path] = uigetfile('.csv', 'Select the association file:', path);
ASSOCIATIONS = readAssociationFile(path, filename);
logpath = fullfile(path, 'logfiles');

% Association fields
VTK_FILE = 1;
CALIBRATION_FILE = 2;
TRANSFORMATION_FILE = 3;
MARKER_LOGFILE = 4;

for i=1:size(ASSOCIATIONS,1)
    
    % Load U2M calibration (variables R, T)
    load(fullfile(path, ASSOCIATIONS{i, CALIBRATION_FILE}));
    R_U2M = R;
    T_U2M = T;
    
    % Find marker position from vtk file
    [stime, etime] = readVTKtimestamp(path, ASSOCIATIONS{i, VTK_FILE});
    [X, Y, Z, q0, qx, qy, qz] = ...
        findAverageMarkerPosition(logpath, ASSOCIATIONS{i, MARKER_LOGFILE}, stime, etime);
    R_M2V = quat2dcmVicra([q0 qx qy qz]);
    T_M2V = [X Y Z];

    % Load U2M calibration (variables R, T, mriName)
    load(fullfile(path, ASSOCIATIONS{i, TRANSFORMATION_FILE}));
    R_V2MRI = R;
    T_V2MRI = T;
    
    % Compute u2mri transformation
    R = R_V2MRI * R_M2V * R_U2M;
    T = (T_U2M * R_M2V') * R_V2MRI' + T_M2V * R_V2MRI' + T_V2MRI;

    % Compute angles VTK (for paraview)
    [rx,ry,rz] = dcm2anglesVTK(R);
    anglesVTK = radtodeg([rx ry rz]);

    % Save files
    [~, stem, ~] = fileparts(ASSOCIATIONS{i, VTK_FILE});
    % ASCII file to be included in the dataset
    saveASCII(fullfile(path, strcat(stem, '_', mriName, '_u2mri_ASCII.mat')), R, T);
    % CSV file for paraview
    csvwrite(fullfile(path, strcat(stem, '_', mriName, '_u2mri.csv')), [T; anglesVTK])

    fprintf(1, 'File "%s" completed.\n', ASSOCIATIONS{i, VTK_FILE});
end
