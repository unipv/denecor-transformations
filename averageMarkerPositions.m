%  Copyright 2017 Universita' degli Studi di Pavia
%  Computer Vision and Multimedia Laboratory
%  http://vision.unipv.it
% 
%  This program is free software; you can redistribute it and/or modify
%  it under the terms of the GNU General Public License as published by
%  the Free Software Foundation; either version 2 of the License, or
%  (at your option) any later version.
% 
%  This program is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
% 
%  You should have received a copy of the GNU General Public License along
%  with this program; if not, write to the Free Software Foundation, Inc.,
% 
%  Authors: giulia.matrone@unipv.it, marco.piastra@unipv.it

% COMPUTE US AVERAGE PROBE POSITIONS IN LOG FILES

function [X, Y, Z, q0, qx, qy, qz] = averageMarkerPositions(varargin)

if nargin > 0
    path = varargin{1};
else
    path = '';
end
if nargin > 1
    filename = varargin{2};
else
    filename = '';
end

[handle1, ~, ~] = parseLogfile(path, filename);


handle1(handle1(:,2) == 1,:) = []; % remove missing records

n_set = unique(handle1(:,10)); %dataset number

id_start = zeros(length(n_set),1);
id_end = zeros(length(n_set),1);
for i = 1:length(n_set)
	id_start(i) = find(handle1(:,10) == n_set(i), 1, 'first');
	id_end(i) = find(handle1(:,10) == n_set(i), 1, 'last');
end

% ACQUIRED DATA (HANDLE 1)
[X, Y, Z, q0, qx, qy, qz] = averageHandlePositions(handle1, id_start, id_end); 

