%  Copyright 2017 Universita' degli Studi di Pavia
%  Computer Vision and Multimedia Laboratory
%  http://vision.unipv.it
%
%  This program is free software; you can redistribute it and/or modify
%  it under the terms of the GNU General Public License as published by
%  the Free Software Foundation; either version 2 of the License, or
%  (at your option) any later version.
%
%  This program is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
%
%  You should have received a copy of the GNU General Public License along
%  with this program; if not, write to the Free Software Foundation, Inc.,
%
%  Authors: giulia.matrone@unipv.it, marco.piastra@unipv.it

%% Parse and save position data acquired with Polaris Vicra

function [handle1, handle2, timestamp] = parseLogfile(varargin)

if nargin > 0
    path = varargin{1};
else
    path = '';
end
if nargin > 1
    filename = varargin{2};
else
    filename = '';
end

% Open Vicra Log-file
if isempty(filename)
    [filename, path] = uigetfile('.txt','Select the Polaris Vicra log file', path);    
end
fid = fopen(fullfile(path, filename),'r');

% Read Vicra Log-file and save serial-communication data
b = textscan(fid,'%s','Delimiter','\n');
data = b{1,1}; 


% Parse data and save only responses between commands 'TSTART' and 'TSTOP'
id_ok = [];
flag = 0;
for i = 1:cellfun('length',b) 
    
    if strfind(data{i},'TSTART') > 0
        flag = 1;
    elseif strfind(data{i},'TSTOP') > 0
        flag = 0;
    end
    if flag == 1
        id_ok = [id_ok; i];
    end
end
id_ok_shift = [id_ok(1); id_ok(1:end-1)];
id_diff = id_ok - id_ok_shift;
id_start = id_ok(id_diff ~= 1);
% id_start(1) = []; %the first TSTART is in the file header
id_stop = id_ok_shift(id_diff ~= 1);
id_stop(1) = []; 
id_stop = [id_stop; id_ok(end)]; 


% Parse data and save only responses to command 'TX0001'
k = 1;
for j = 1:length(id_start)
    
    for i = id_start(j):id_stop(j)
        
        if strfind(data{i},'TX:0001') > 0
            % Save the data of object-handles no.1-2 in each cell of array 'id'
            id{k} = strcat(data{i+2},data{i+3});
            % dataset number
            II(k) = j;
            k = k+1;
        end
        
    end
    
end

N = length(id);

% Initialize the new variables
timestamp = cell(N,1); % timestamp array
handle1 = zeros(N,10); % parsed-data array for handle 01: handle no., missing (0=no/1=yes), Q0, Qx, Qy, Qz, Tx, Ty, Tz, dataset no.
handle2 = zeros(N,10); % parsed-data array for handle 02: handle no., missing (0=no/1=yes), Q0, Qx, Qy, Qz, Tx, Ty, Tz, dataset no.

for i=1:N

    tmp = id{i}; %temporary string
    
    % Save timestamp
    start_idx = strfind(tmp,'<');
    timestamp{i} = tmp(1:start_idx-1);
    
    % Save handle number (for handle 1)
    start_idx = start_idx+4;
    handle1(i,1) = str2num(tmp(start_idx+[0:1]));
    
    % Save 'MISSING' or position data (for handle 1)
    start_idx = start_idx+2;
    if tmp(start_idx) == 'M'
        handle1(i,2) = 1;
        start_idx = start_idx+23;
    else
        handle1(i,2) = 0;
        handle1(i,3) = str2num(tmp(start_idx+[0:5]))/10000; %Q0
        handle1(i,4) = str2num(tmp(start_idx+[6:11]))/10000; %Qx
        handle1(i,5) = str2num(tmp(start_idx+[12:17]))/10000; %Qy
        handle1(i,6) = str2num(tmp(start_idx+[18:23]))/10000; %Qz
        handle1(i,7) = str2num(tmp(start_idx+[24:30]))/100; %Tx
        handle1(i,8) = str2num(tmp(start_idx+[31:37]))/100; %Ty
        handle1(i,9) = str2num(tmp(start_idx+[38:44]))/100; %Tz
        start_idx = start_idx+45+22;
    end
    handle1(i,10) = II(i);
    
    % Save handle number (for handle 2)
    handle2(i,1) = str2num(tmp(start_idx+[0:1]));
    
    % Save 'MISSING' or position data (for handle 2)
    start_idx = start_idx+2;
    if tmp(start_idx) == 'M'
        handle2(i,2) = 1;
        start_idx = start_idx+23;
    else
        handle2(i,2) = 0;
        handle2(i,3) = str2num(tmp(start_idx+[0:5]))/10000; %Q0
        handle2(i,4) = str2num(tmp(start_idx+[6:11]))/10000; %Qx
        handle2(i,5) = str2num(tmp(start_idx+[12:17]))/10000; %Qy
        handle2(i,6) = str2num(tmp(start_idx+[18:23]))/10000; %Qz
        handle2(i,7) = str2num(tmp(start_idx+[24:30]))/100; %Tx
        handle2(i,8) = str2num(tmp(start_idx+[31:37]))/100; %Ty
        handle2(i,9) = str2num(tmp(start_idx+[38:44]))/100; %Tz
        start_idx = start_idx+45+22;
    end
    handle2(i,10) = II(i);
    
end
