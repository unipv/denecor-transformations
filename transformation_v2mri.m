%  Copyright 2017 Universita' degli Studi di Pavia
%  Computer Vision and Multimedia Laboratory
%  http://vision.unipv.it
% 
%  This program is free software; you can redistribute it and/or modify
%  it under the terms of the GNU General Public License as published by
%  the Free Software Foundation; either version 2 of the License, or
%  (at your option) any later version.
% 
%  This program is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
% 
%  You should have received a copy of the GNU General Public License along
%  with this program; if not, write to the Free Software Foundation, Inc.,
% 
%  Authors: giulia.matrone@unipv.it, marco.piastra@unipv.it

%% Transformation from Vicra to MRI space (V2MRI)

if size(dbstack,1)==1
    % Direct call
    clear global
    clear variables
    close all
    clc
end

DEBUG = 1;

[filename, path] = uigetfile('.csv', 'Select the association file:', path);
ASSOCIATIONS = readAssociationFile(path, filename);
logpath = fullfile(path, 'logfiles');

% Association fields
VTK_FILE = 1;
POINTER_LOGFILE = 2;

% Load offset vector for the pointer tool
OFFSET = csvread(fullfile(logpath, 'pivoting.csv')); 

U2M_TRANSFORMS = zeros(size(ASSOCIATIONS,1),7);
    
% Load position of spheres in pattern
mriName = ASSOCIATIONS{1, VTK_FILE};
    
P0S0=csvread(fullfile(path, strcat(mriName, '_P0S0.csv')),1,0);
P0S0=P0S0(3:5);
P0S1=csvread(fullfile(path, strcat(mriName, '_P0S1.csv')),1,0);
P0S1=P0S1(3:5);
P0S2=csvread(fullfile(path, strcat(mriName, '_P0S2.csv')),1,0);
P0S2=P0S2(3:5);

P1S0=csvread(fullfile(path, strcat(mriName, '_P1S0.csv')),1,0);
P1S0=P1S0(3:5);
P1S1=csvread(fullfile(path, strcat(mriName, '_P1S1.csv')),1,0);
P1S1=P1S1(3:5);
P1S2=csvread(fullfile(path, strcat(mriName, '_P1S2.csv')),1,0);
P1S2=P1S2(3:5);

P2S0=csvread(fullfile(path, strcat(mriName, '_P2S0.csv')),1,0);
P2S0=P2S0(3:5);
P2S1=csvread(fullfile(path, strcat(mriName, '_P2S1.csv')),1,0);
P2S1=P2S1(3:5);
P2S2=csvread(fullfile(path, strcat(mriName, '_P2S2.csv')),1,0);
P2S2=P2S2(3:5);

P3S0=csvread(fullfile(path, strcat(mriName, '_P3S0.csv')),1,0);
P3S0=P3S0(3:5);
P3S1=csvread(fullfile(path, strcat(mriName, '_P3S1.csv')),1,0);
P3S1=P3S1(3:5);
P3S2=csvread(fullfile(path, strcat(mriName, '_P3S2.csv')),1,0);
P3S2=P3S2(3:5);

P4S0=csvread(fullfile(path, strcat(mriName, '_P4S0.csv')),1,0);
P4S0=P4S0(3:5);
P4S1=csvread(fullfile(path, strcat(mriName, '_P4S1.csv')),1,0);
P4S1=P4S1(3:5);
P4S2=csvread(fullfile(path, strcat(mriName, '_P4S2.csv')),1,0);
P4S2=P4S2(3:5);

P5S0=csvread(fullfile(path, strcat(mriName, '_P5S0.csv')),1,0);
P5S0=P5S0(3:5);
P5S1=csvread(fullfile(path, strcat(mriName, '_P5S1.csv')),1,0);
P5S1=P5S1(3:5);
P5S2=csvread(fullfile(path, strcat(mriName, '_P5S2.csv')),1,0);
P5S2=P5S2(3:5);

% Parse logfile to obtain (avg) pointer positions
logfilename = ASSOCIATIONS{1, POINTER_LOGFILE};
[X, Y, Z, q0, qx, qy, qz] = averageMarkerPositions(logpath, logfilename);

if (length(X) ~= 18)
    error(['The logfile ' logfilename ' should contain 18 positions']);
end
    
% Apply offset to obtain pointer positions
MARKER_DATA = [X Y Z q0 qx qy qz];
POSITIONS = zeros(length(X),3); 
for j=1:length(X)
    T = MARKER_DATA(j,1:3);
    R = quat2dcmVicra(MARKER_DATA(j,4:7));
    POSITIONS(j,:) = OFFSET * R' + T;
end

% Compute v2mri transformations 
A = POSITIONS';
B = [P0S0' P0S1' P0S2' P1S0' P1S1' P1S2' P2S0' P2S1' P2S2' ...
    P3S0' P3S1' P3S2' P4S0' P4S1' P4S2' P5S0' P5S1' P5S2'];

[~, R, T, err] = absoluteOrientationQuaternion( A, B, 0);
fprintf(1, 'File "%s": error = %d\n', filename, err);
T = T';

% For visualization in Paraview
[rx,ry,rz] = dcm2anglesVTK(R);
anglesVTK = radtodeg([rx ry rz]);

[~, stem, ~] = fileparts(filename);
save(fullfile(path, strcat(stem, '_v2mri.mat')), 'R', 'T', 'err', 'anglesVTK', 'mriName');
saveASCII(fullfile(path, strcat(stem, '_ASCII_v2mri.mat')), R, T);

if (DEBUG)
    for i = 1:length(X)
        writePLYDart(fullfile(path, strcat(stem, '-', num2str(i), '.ply')), ...
            OFFSET, ...
            quat2dcmVicra([q0(i) qx(i) qy(i) qz(i)]), [X(i) Y(i) Z(i)], 10);
    end
end


