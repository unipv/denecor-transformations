%  Copyright 2017 Universita' degli Studi di Pavia
%  Computer Vision and Multimedia Laboratory
%  http://vision.unipv.it
% 
%  This program is free software; you can redistribute it and/or modify
%  it under the terms of the GNU General Public License as published by
%  the Free Software Foundation; either version 2 of the License, or
%  (at your option) any later version.
% 
%  This program is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
% 
%  You should have received a copy of the GNU General Public License along
%  with this program; if not, write to the Free Software Foundation, Inc.,
% 
%  Authors: giulia.matrone@unipv.it, marco.piastra@unipv.it

%% Calibration from multiple acquistions

if size(dbstack,1)==1
    % Direct call
    clear global
    clear variables
    close all
    clc
end

[filename, path] = uigetfile('.csv', 'Select the association file:', path);
ASSOCIATIONS = readAssociationFile(path, filename);
logpath = fullfile(path, 'logfiles');

% Association fields
VTK_FILE = 1;
POINTER_LOGFILE = 2;
MARKER_LOGFILE = 3;

% Load offset vector for the pointer tool
OFFSET = csvread(fullfile(logpath, 'pivoting.csv')); 

U2M_TRANSFORMS = zeros(size(ASSOCIATIONS,1),7);
for i=1:size(ASSOCIATIONS,1)
    
    % Load position of spheres in pattern
    [~, stem, ~] = fileparts(ASSOCIATIONS{i, VTK_FILE});
    S0=csvread(fullfile(path, strcat(stem, '_S0.csv')),1,0);
    S0=S0(3:5);
    S1=csvread(fullfile(path, strcat(stem, '_S1.csv')),1,0);
    S1=S1(3:5);
    S2=csvread(fullfile(path, strcat(stem, '_S2.csv')),1,0);
    S2=S2(3:5);

    % Parse logfile to obtain (avg) pointer positions
    logfilename = ASSOCIATIONS{i, POINTER_LOGFILE};
    [X, Y, Z, q0, qx, qy, qz] = averageMarkerPositions(logpath, logfilename);
    
    if (length(X) ~= 3)
        error(['The logfile ' logfilename ' should contain 3 positions']);
    end
    
    % Apply offset to obtain pointer positions
    MARKER_DATA = [X Y Z q0 qx qy qz];
    POSITIONS = zeros(length(X),3); 
    for j=1:length(X)
        T = MARKER_DATA(j,1:3);
        R = quat2dcmVicra(MARKER_DATA(j,4:7));
        POSITIONS(j,:) = OFFSET * R' + T;
    end

    % Compute u2v transformations 
    A = [S0' S1' S2' S0'];
    B = [POSITIONS(1:3,:)' POSITIONS(1,:)'];
    [~, R, T, err] = absoluteOrientationQuaternion( A, B, 0);
    fprintf(1, 'File "%s": error = %d\n', ASSOCIATIONS{i, VTK_FILE}, err);
    R_U2V = R;
    T_U2V = T';
    
    % Find marker position from vtk file
    [stime, etime] = readVTKtimestamp(path, ASSOCIATIONS{i, VTK_FILE});
    [X, Y, Z, q0, qx, qy, qz] = ...
        findAverageMarkerPosition(logpath, ASSOCIATIONS{i, MARKER_LOGFILE}, stime, etime);
    R_M2V = quat2dcmVicra([q0 qx qy qz]);
    T_M2V = [X Y Z];

    % Specific u2m transformation
    R_U2M = R_M2V' * R_U2V;
    T_U2M = (T_U2V - T_M2V) * R_M2V;
    
    U2M_TRANSFORMS(i,:) = [T_U2M dcm2quatVicra(R_U2M)];
end

% Average u2m transformations and save the results
Q_U2M = avg_quaternion_markley(U2M_TRANSFORMS(:,4:7));

R = quat2dcmVicra(Q_U2M');
T = mean(U2M_TRANSFORMS(:,1:3), 1);

[~, stem, ~] = fileparts(filename);
save(fullfile(path, strcat(stem, '_u2m.mat')), 'R', 'T');
saveASCII(fullfile(path, strcat(stem, '_ASCII_u2m.mat')), R, T);
