%  Copyright 2017 Universita' degli Studi di Pavia
%  Computer Vision and Multimedia Laboratory
%  http://vision.unipv.it
% 
%  This program is free software; you can redistribute it and/or modify
%  it under the terms of the GNU General Public License as published by
%  the Free Software Foundation; either version 2 of the License, or
%  (at your option) any later version.
% 
%  This program is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
% 
%  You should have received a copy of the GNU General Public License along
%  with this program; if not, write to the Free Software Foundation, Inc.,
% 
%  Authors: giulia.matrone@unipv.it, marco.piastra@unipv.it


function [rx ry rz] = dcm2anglesVTK(R)
% function [rx ry rz] = dcm2anglesVTK(R)
% converts a rotation matrix to extrinsic rotation angles (VTK conventions)
% angles are expressed in radians, to be used in Paraview

% The conversion method comes from file vtkTransform.cxx

VTK_AXIS_EPSILON = 0.001;

%% first rotate about y axis
x2 = R(3,1);
y2 = R(3,2);
z2 = R(3,3);

x3 = R(2,1);
y3 = R(2,2);
z3 = R(2,3);

d1 = sqrt(x2*x2 + z2*z2);

if (d1 < VTK_AXIS_EPSILON)
    cosTheta = 1.0;
    sinTheta = 0.0;
else
    cosTheta = z2/d1;
    sinTheta = x2/d1;
end

ry = -atan2(sinTheta, cosTheta);

%% now rotate about x axis
d = sqrt(x2*x2 + y2*y2 + z2*z2);

if (d < VTK_AXIS_EPSILON)
    sinPhi = 0.0;
    cosPhi = 1.0;
else
    if (d1 < VTK_AXIS_EPSILON)
        sinPhi = y2/d;
        cosPhi = z2/d;
    else
        sinPhi = y2/d;
        cosPhi = (x2*x2 + z2*z2)/(d1*d);
    end
end

rx = atan2(sinPhi, cosPhi);

%% finally, rotate about z
x3p = x3*cosTheta - z3*sinTheta;
y3p = - sinPhi*sinTheta*x3 + cosPhi*y3 - sinPhi*cosTheta*z3;
d2 = sqrt(x3p*x3p + y3p*y3p);

if (d2 < VTK_AXIS_EPSILON)
    cosAlpha = 1.0;
    sinAlpha = 0.0;
else
    cosAlpha = y3p/d2;
    sinAlpha = x3p/d2;
end
    
rz = atan2(sinAlpha, cosAlpha);
