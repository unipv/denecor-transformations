%  Copyright 2017 Universita' degli Studi di Pavia
%  Computer Vision and Multimedia Laboratory
%  http://vision.unipv.it
% 
%  This program is free software; you can redistribute it and/or modify
%  it under the terms of the GNU General Public License as published by
%  the Free Software Foundation; either version 2 of the License, or
%  (at your option) any later version.
% 
%  This program is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
% 
%  You should have received a copy of the GNU General Public License along
%  with this program; if not, write to the Free Software Foundation, Inc.,
% 
%  Authors: giulia.matrone@unipv.it, marco.piastra@unipv.it

function [stime, etime] = readVTKtimestamp(varargin)

if nargin > 0
    path = varargin{1};
else
    path = '';
end
if nargin > 1
    filename = varargin{2};
else
    filename = '';
end

if isempty(filename)    
    [filename, path] = uigetfile('.vtk','Select the VTK file', path);        
end
fid = fopen(fullfile(path, filename),'r');


str = fgetl(fid);
if not(strcmp(str(3:5), 'vtk'))
   error('This does not appear to be a valid VTK file (in ASCII format)'); 
end

str = fgetl(fid);
if not(strcmp(str(1:9), 'Timestamp'))
   error('This file does not contain a timestamp'); 
end

timestamp = str(12:end);
b = strfind(timestamp,' ');
if b > 0
    stime = timestamp(1:b-1);
    etime = timestamp(b+1:end);
else
   error('Wrong timestamp format');     
end

fclose(fid);

