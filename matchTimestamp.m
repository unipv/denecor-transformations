%  Copyright 2017 Universita' degli Studi di Pavia
%  Computer Vision and Multimedia Laboratory
%  http://vision.unipv.it
%
%  This program is free software; you can redistribute it and/or modify
%  it under the terms of the GNU General Public License as published by
%  the Free Software Foundation; either version 2 of the License, or
%  (at your option) any later version.
%
%  This program is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
%
%  You should have received a copy of the GNU General Public License along
%  with this program; if not, write to the Free Software Foundation, Inc.,
%
%  Authors: giulia.matrone@unipv.it, marco.piastra@unipv.it

function [id_start, id_end] = matchTimestamp(timeseries, stime, etime)
% timeseries = all timestamps
% stime etime = target timestamp (interval endpoints)
% id_start,id_end = indexes of the records that match the desired time window

id_start = find(lexcmp(timeseries, stime) >= 0, 1, 'first');
if (id_start > 1)
    % both extremes have to be included
    id_start = id_start - 1;
end

id_end = find(lexcmp(timeseries, etime) >= 0, 1, 'first');

if isempty(id_start) || id_start == id_end
    error('No compatible interval in this time series for the given endpoints');
end

if isempty(id_end) 
    id_end = length(timeseries);
end

end